Summary: C/C++ Cross Compiler Toolchain
Name: gcc-cross
Version: 1.0
Release: 7
# libgcc, libgfortran, libmudflap, libgomp, libstdc++ and crtstuff have
# GCC Runtime Exception.
License: GPLv3+ and GPLv3+ with exceptions and GPLv2+ with exceptions and LGPLv2+ and BSD
URL: http://gcc.gnu.org

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: binutils, gettext, gmp-devel, isl-devel, libmpc-devel, mpfr-devel, python3-sphinx, sharutils
BuildRequires: audit-libs-devel, gd-devel, libcap-devel, libpng-devel, libselinux-devel, libstdc++-static, systemtap-sdt-devel, valgrind
BuildRequires: jansson-devel
BuildRequires: gdb, native-turbo-tools, gcc, gcc-c++, make, git, flex, bison, rpm-build, automake, autoconf
BuildRequires: libtool, ncurses-devel, bc, zlib-devel, openssl-devel, texinfo, rsync, chrpath, git-lfs
ExclusiveArch: i386 x86_64

%description
C/C++ Cross Compiler Toolchain

%prep
mkdir -p %{_builddir}/%{name}-%{version}
cd %{_builddir}/%{name}-%{version}
cp -r %{_sourcedir}/* ./

%build
cd %{_builddir}/%{name}-%{version}
files=$(ls *.tar 2> /dev/null | wc -l)
if [ "$files" != 0 ]; then
	ls *.tar | xargs -n1 tar xvf
fi
bash download.sh
bash build.sh gcc_arm64le &
bash build.sh gcc_arm32le
wait

%install
mkdir -p %{buildroot}/tmp
cp %{_builddir}/output/gcc_arm64le/gcc_arm64le.tar.gz %{buildroot}/tmp
cp %{_builddir}/output/gcc_arm32le/gcc_arm32le.tar.gz %{buildroot}/tmp

%files
%attr(755, root, root) /tmp/gcc_arm64le.tar.gz
%attr(755, root, root) /tmp/gcc_arm32le.tar.gz

%changelog
* Tue Feb 11 2025 zhengchenhui <zhengchenhui1@huawei.com> - 1.0-7
-Type:Fix
-ID:NA
-SUG:NA
-DESC: Fix binutils download error.

* Mon Mar 11 2024 zhengchenhui <zhengchenhui1@huawei.com> - 1.0-6
-Type:Fix
-ID:NA
-SUG:NA
-DESC: Fix cross-compilation tool chain

* Mon Aug 07 2023 dingguangya <dingguangya1@huawei.com> - 1.0-5
-Type:Fix
-ID:NA
-SUG:NA
-DESC: Fix cross-compilation tool chain

* Mon Mar 06 2023 dingguangya <dingguangya1@huawei.com> - 1.0-4
-Type:Fix
-ID:NA
-SUG:NA
-DESC: Update cross-compilation tool chain

* Mon Nov 28 2022 dingguangya <dingguangya1@huawei.com> - 1.0-3
-Type:Fix
-ID:NA
-SUG:NA
-DESC: Fix the hard coding problem in the software package building process

* Thu Nov 17 2022 dingguangya <dingguangya1@huawei.com> - 1.0-2
-Type:Fix
-ID:NA
-SUG:NA
-DESC: Unified changelog/Release field format

* Tue Nov 8 2022 dingguangya <dingguangya1@huawei.com> - 1.0-1
- Type:Fix
- ID:NA
- SUG:NA
- DESC: Increase parallel compilation capability

* Wed Mar 23 2022 dingguangya <dingguangya1@huawei.com> - 1.0-0 
- Type:Init
- ID:NA
- SUG:NA
- DESC: Init gcc-cross repository
