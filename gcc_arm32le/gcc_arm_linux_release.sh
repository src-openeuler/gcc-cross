#!/bin/bash

set -e
set -x

# Target root directory,should be adapted according to your environment
readonly ROOT_TAR_DIR="$PWD/arm32le_build_dir"
readonly ROOT_TAR_SRC="$PWD/../../open_source"
readonly INSTALL="gcc_arm32le"
readonly PREFIX="$PWD/arm32le_build_dir/$INSTALL"
readonly OUTPUT="$PWD/../../output/$INSTALL"

readonly SECURE_LDFLAGS="-z relro -z now -z noexecstack"
readonly SECURE_CFLAGS="-O2 -Wall -fstack-protector-strong -D_FORTIFY_SOURCE=2 -Wl,-z,relro,-z,now,-z,noexecstack -Wtrampolines -Wno-stringop-overflow  -fPIE"
readonly SECURE_CXXFLAGS="-O2 -Wall -fstack-protector-strong -D_FORTIFY_SOURCE=2 -Wl,-z,relro,-z,now,-z,noexecstack -Wtrampolines -Wno-stringop-overflow -fPIE"
readonly SECURE_CFLAGS_FOR_TARGET="-O2 -Wall -fstack-protector-strong -D_FORTIFY_SOURCE=2 -Wl,-z,relro,-z,now,-z,noexecstack -Wtrampolines -Wno-stringop-overflow -march=armv7-a -mfloat-abi=soft -mfpu=vfp"
readonly SECURE_CXXFLAGS_FOR_TARGET="-O2 -Wall -fstack-protector-strong -D_FORTIFY_SOURCE=2 -Wl,-z,relro,-z,now,-z,noexecstack -Wtrampolines -Wno-stringop-overflow -march=armv7-a -mfloat-abi=soft -mfpu=vfp"

readonly TARGET="arm-linux-gnueabi"
readonly HOST="$(gcc -dumpmachine)"
readonly BUILD="$(gcc -dumpmachine)"

readonly SYSROOT="$PREFIX/sysroot"

#OTHER CONFIG
readonly ARCH="armv7-a"

if [[ -z "$COMPILER_INFO" ]];then
	source $PWD/../config.xml
fi

declare -x CFLAGS_FOR_TARGET="-O2 -Wall -fstack-protector-strong -D_FORTIFY_SOURCE=2 -Wl,-z,relro,-z,now,-z,noexecstack -Wtrampolines -Wno-stringop-overflow -march=armv7-a -mfloat-abi=soft -mfpu=vfp"
declare -x CXXFLAGS_FOR_TARGET="$CFLAGS_FOR_TARGET"

#Create an empty file
create_new_file(){
while [ $# != 0 ] ; do
[ -n "$1" ] && rm -rf $1 ; mkdir -p $1; shift; done }

create_new_file $ROOT_TAR_DIR/obj $PREFIX $OUTPUT $ROOT_TAR_DIR/obj/build-gmp $ROOT_TAR_DIR/obj/build-mpfr $ROOT_TAR_DIR/obj/build-isl $ROOT_TAR_DIR/obj/build-mpc $ROOT_TAR_DIR/obj/build-binutils $ROOT_TAR_DIR/obj/build-gcc-first $ROOT_TAR_DIR/obj/glibc-headers $ROOT_TAR_DIR/obj/build-gcc-second $ROOT_TAR_DIR/obj/build-eglibc $ROOT_TAR_DIR/obj/build-gcc-final

# Change libstdc++.so option
sed -i "s#^\\t\$(OPT_LDFLAGS).*#\\t\$(OPT_LDFLAGS) \$(SECTION_LDFLAGS) \$(AM_CXXFLAGS)  \$(LTLDFLAGS) -Wl,-z,relro,-z,now,-z,noexecstack -Wtrampolines -o \$\@#g" $ROOT_TAR_SRC/$GCC/$GCC_DIR/libstdc++-v3/src/Makefile.in

echo "Building gmp..." && pushd $ROOT_TAR_DIR/obj/build-gmp
LDFLAGS="${SECURE_LDFLAGS}" CFLAGS="${SECURE_CFLAGS}" CXXFLAGS="${SECURE_CXXFLAGS}" CFLAGS_FOR_TARGET="${SECURE_CFLAGS_FOR_TARGET}" CXXFLAGS_FOR_TARGET="${SECURE_CXXFLAGS_FOR_TARGET}"  $ROOT_TAR_SRC/$GMP/$GMP_DIR/configure --build=$BUILD --prefix=$PREFIX --disable-shared --host=$HOST --enable-cxx
make -j 32 && make install && popd

echo "Building mpfr..." && pushd $ROOT_TAR_DIR/obj/build-mpfr
LDFLAGS="${SECURE_LDFLAGS}" CFLAGS="${SECURE_CFLAGS}" CXXFLAGS="${SECURE_CXXFLAGS}" CFLAGS_FOR_TARGET="${SECURE_CFLAGS_FOR_TARGET}" CXXFLAGS_FOR_TARGET="${SECURE_CXXFLAGS_FOR_TARGET}"  $ROOT_TAR_SRC/$MPFR/$MPFR_DIR/configure --build=$BUILD --target=$TARGET --prefix=$PREFIX --disable-shared --host=$HOST --with-gmp=$PREFIX
make -j 32 && make install && popd

echo "Building isl..." && pushd $ROOT_TAR_DIR/obj/build-isl
LDFLAGS="${SECURE_LDFLAGS}" CFLAGS="${SECURE_CFLAGS}" CXXFLAGS="${SECURE_CXXFLAGS}" CFLAGS_FOR_TARGET="${SECURE_CFLAGS_FOR_TARGET}" CXXFLAGS_FOR_TARGET="${SECURE_CXXFLAGS_FOR_TARGET}"  $ROOT_TAR_SRC/$ISL/$ISL_DIR/configure --build=$BUILD --host=$HOST --prefix=$PREFIX --with-gmp-prefix=$PREFIX --disable-shared
make -j 32 && make install && popd

echo "Building mpc..." && pushd $ROOT_TAR_DIR/obj/build-mpc
LDFLAGS="${SECURE_LDFLAGS}" CFLAGS="${SECURE_CFLAGS}" CXXFLAGS="${SECURE_CXXFLAGS}" CFLAGS_FOR_TARGET="${SECURE_CFLAGS_FOR_TARGET}" CXXFLAGS_FOR_TARGET="${SECURE_CXXFLAGS_FOR_TARGET}"  $ROOT_TAR_SRC/$MPC/$MPC_DIR/configure --build=$BUILD --target=$TARGET --prefix=$PREFIX --disable-shared --host=$HOST --with-gmp=$PREFIX
make -j 32 && make install && popd

echo "Building binutils..." && pushd $ROOT_TAR_DIR/obj/build-binutils
LDFLAGS="${SECURE_LDFLAGS}" CFLAGS="${SECURE_CFLAGS}" CXXFLAGS="${SECURE_CXXFLAGS}" CFLAGS_FOR_TARGET="${SECURE_CFLAGS_FOR_TARGET}" CXXFLAGS_FOR_TARGET="${SECURE_CXXFLAGS_FOR_TARGET}"  $ROOT_TAR_SRC/$BINUTILS/$BINUTILS_DIR/configure --build=$BUILD --target=$TARGET --prefix=$PREFIX --host=$HOST --with-pkgversion="${COMPILER_INFO}" --with-sysroot=$SYSROOT --enable-plugins --enable-ld=yes --disable-gdb --libdir=$PREFIX/lib
make -j 32 && make install prefix=$PREFIX exec_prefix=$PREFIX libdir=$PREFIX/lib && popd

echo "Installing kernel headers..." && pushd $ROOT_TAR_SRC/$KERNEL_HEADERS/
mkdir -p $SYSROOT/usr/ && cp -r include $SYSROOT/usr/
mkdir -p $SYSROOT/usr/include/bits $SYSROOT/usr/include/gnu && popd

echo "Building GCC first..." && pushd $ROOT_TAR_DIR/obj/build-gcc-first
LDFLAGS="${SECURE_LDFLAGS}" CFLAGS="${SECURE_CFLAGS}" CXXFLAGS="${SECURE_CXXFLAGS}" CFLAGS_FOR_TARGET="${SECURE_CFLAGS_FOR_TARGET}" CXXFLAGS_FOR_TARGET="${SECURE_CXXFLAGS_FOR_TARGET}"  $ROOT_TAR_SRC/$GCC/$GCC_DIR/configure --build=$BUILD --host=$HOST --target=$TARGET --prefix=$PREFIX --disable-multilib --disable-libmudflap --without-headers --with-newlib --enable-threads=no --disable-shared --disable-nls --with-arch=$ARCH --with-gnu-as --with-gnu-ld --enable-poison-system-directories --disable-libssp --enable-symvers=gnu --enable-__cxa_atexit --disable-libgomp --disable-libstdcxx-pch --disable-libquadmath --disable-lto --enable-languages=c --with-pkgversion="${COMPILER_INFO}" --with-gmp=$PREFIX --with-mpfr=$PREFIX --with-mpc=$PREFIX --with-isl=$PREFIX --with-sysroot=$SYSROOT --with-build-sysroot=$SYSROOT --with-build-time-tools=$PREFIX/$TARGET/bin --libdir=$PREFIX/lib --disable-bootstrap --disable-libatomic --disable-libcilkrts --disable-libstdcxx-dual-abi --with-system-zlib
make -j 32 CPPFLAGS_FOR_TARGET=--sysroot=$SYSROOT && make install prefix=$PREFIX exec_prefix=$PREFIX libdir=$PREFIX/lib && popd

echo "Installing eglibc headers for little-endian..." && mkdir -p $SYSROOT/lib/ $SYSROOT/usr/lib/ && pushd "$ROOT_TAR_DIR/obj/glibc-headers"
cp -f $ROOT_TAR_SRC/$GLIBC/$GLIBC_DIR/include/gnu/stubs.h $SYSROOT/usr/include/gnu
CC="$PREFIX/bin/$TARGET-gcc $CFLAGS_FOR_TARGET" $ROOT_TAR_SRC/$GLIBC/$GLIBC_DIR/configure --prefix=/usr --with-headers=$SYSROOT/usr/include --build=$BUILD --host=$TARGET --disable-profile --without-gd --without-cvs --disable-multi-arch --enable-add-ons libc_cv_forced_unwind=yes libc_cv_c_cleanup=yes --with-float=soft --with-fp=no --enable-stack-protector=strong libc_cv_ssp=yes
make install-bootstrap-headers=yes install-headers install_root=$SYSROOT && make -j 32 csu/subdir_lib
cp csu/crt1.o csu/crti.o csu/crtn.o $SYSROOT/usr/lib/
$PREFIX/bin/$TARGET-gcc $CFLAGS_FOR_TARGET -o "$SYSROOT/usr/lib/libc.so" -nostdlib -nostartfiles -shared -x c /dev/null && popd

echo "Building GCC second..." && pushd $ROOT_TAR_DIR/obj/build-gcc-second
# Fix this issue https://gitee.com/src-openeuler/gcc-cross/issues/I7KWH4. The reason is that gcc_arm32le/sysroot/usr/include/bits/stdio_lim.h is generated after glibc is upgraded. 
#cp $ROOT_TAR_DIR/obj/glibc-headers/bits/stdio_lim.h $SYSROOT/usr/include/bits
LDFLAGS="${SECURE_LDFLAGS}" CFLAGS="${SECURE_CFLAGS}" CXXFLAGS="${SECURE_CXXFLAGS}" CFLAGS_FOR_TARGET="${SECURE_CFLAGS_FOR_TARGET}" CXXFLAGS_FOR_TARGET="${SECURE_CXXFLAGS_FOR_TARGET}"  $ROOT_TAR_SRC/$GCC/$GCC_DIR/configure --build=$BUILD --host=$HOST --target=$TARGET --prefix=$PREFIX --disable-multilib --disable-libmudflap --enable-shared --with-arch=$ARCH --enable-__cxa_atexit --with-gnu-as --with-gnu-ld --disable-nls --disable-libssp --disable-libgomp --enable-lto --disable-libquadmath --enable-poison-system-directories --enable-symvers=gnu --enable-languages=c --with-pkgversion="${COMPILER_INFO}" --with-sysroot=$SYSROOT --with-build-sysroot=$SYSROOT --with-gmp=$PREFIX --with-mpfr=$PREFIX --with-mpc=$PREFIX --with-isl=$PREFIX --with-build-time-tools=$PREFIX/$TARGET/bin --libdir=$PREFIX/lib --disable-bootstrap --disable-libatomic --disable-libcilkrts --disable-libstdcxx-dual-abi --with-system-zlib
make -j 32 CPPFLAGS_FOR_TARGET=--sysroot=$SYSROOT build_tooldir=$PREFIX/$TARGET && make install-gcc install-target-libgcc prefix=$PREFIX exec_prefix=$PREFIX libdir=$PREFIX/lib && popd

echo "Building eglibc for little-endian..." && mkdir -p $SYSROOT/lib/ $SYSROOT/usr/lib/ && pushd $ROOT_TAR_DIR/obj/build-eglibc
CC="$PREFIX/bin/$TARGET-gcc $CFLAGS_FOR_TARGET" CXX="$PREFIX/bin/$TARGET-g++ $CXXFLAGS_FOR_TARGET" LDFLAGS="${SECURE_LDFLAGS}" $ROOT_TAR_SRC/$GLIBC/$GLIBC_DIR/configure --prefix=/usr --with-headers=$SYSROOT/usr/include --build=$BUILD --host=$TARGET --disable-profile --without-gd --enable-obsolete-rpc --without-cvs --with-__thread --with-tls --disable-multi-arch --enable-shared --with-fp --enable-add-ons libc_cv_forced_unwind=yes libc_cv_c_cleanup=yes --libdir=/usr/lib --with-float=soft --with-fp=no --enable-stack-protector=strong libc_cv_ssp=yes
make -j 32 && make install_root=$SYSROOT slibdir=/lib rtlddir=/lib install && popd

echo "Building GCC final..." && pushd $ROOT_TAR_DIR/obj/build-gcc-final
LDFLAGS="${SECURE_LDFLAGS}" CFLAGS="${SECURE_CFLAGS}" CXXFLAGS="${SECURE_CXXFLAGS}" CFLAGS_FOR_TARGET="${SECURE_CFLAGS_FOR_TARGET}" CXXFLAGS_FOR_TARGET="${SECURE_CXXFLAGS_FOR_TARGET}"  $ROOT_TAR_SRC/$GCC/$GCC_DIR/configure --build=$BUILD --host=$HOST --target=$TARGET --with-arch=$ARCH --prefix=$PREFIX --disable-multilib --enable-__cxa_atexit --with-gnu-as --with-gnu-ld --disable-libmudflap --enable-libgomp --disable-libquadmath --enable-threads --enable-plugin --enable-c99 --enable-long-long --disable-nls --enable-shared --enable-poison-system-directories --enable-symvers=gnu --with-pkgversion="${COMPILER_INFO}" --enable-languages=c,c++,fortran --with-headers=$SYSROOT/usr/include --with-sysroot=$SYSROOT --with-build-sysroot=$SYSROOT --with-gmp=$PREFIX --with-mpfr=$PREFIX --with-mpc=$PREFIX --with-isl=$PREFIX --with-build-time-tools=$PREFIX/$TARGET/bin --libdir=$PREFIX/lib --disable-bootstrap --disable-libcilkrts --disable-libstdcxx-dual-abi --enable-default-pie --with-system-zlib
make -j 32 CPPFLAGS_FOR_TARGET=--sysroot=$SYSROOT && make install prefix=$PREFIX exec_prefix=$PREFIX libdir=$PREFIX/lib && popd

# handle rpath
for file in $(find "$PREFIX" -name "lib*so*.0") ; do
	chrpath --delete $file
done

for file in $(find "$PREFIX/sysroot/usr/lib/gconv" -name "*so") ; do
        chrpath --delete $file
done

for file in $(find "$PREFIX" -name "*") ; do
    if [ -f "$file" ]
    then
        result=$(file $file)
        res=$(echo $result | grep "ELF" | cat)
        if [[ "$res" != "" ]]
        then
                get_arch=`arch`
                if [[ $get_arch =~ "x86_64" ]]
                then
                        res1=$(echo $result | grep "ARM" | cat)
                        if [[ "$res1" != "" ]]
                        then
                                $PREFIX/bin/$TARGET-strip $file
                        else
                                strip $file
                        fi
                else
                        strip $file
                fi
        fi
   fi
done

pushd $PREFIX/.. && chmod 750 $INSTALL -R
tar --format=gnu -czf $OUTPUT/$INSTALL.tar.gz $INSTALL && popd

echo "Build Complete!" && echo "To use this newly-built $INSTALL cross toolchain, add $PREFIX/bin to your PATH!"
