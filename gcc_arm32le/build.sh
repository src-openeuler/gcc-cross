#!/bin/bash

set -e
set -x

readonly LOG_PATH="$PWD/../logs"

if [[ -z "$COMPILER_INFO" ]];then
	source $PWD/../config.xml
fi

[ -e "$LOG_PATH/gcc_arm32le_patch.log" ] && rm $LOG_PATH/gcc_arm32le_patch.log
[ -e "$LOG_PATH/gcc_arm32le_build.log" ] && rm $LOG_PATH/gcc_arm32le_build.log
mkdir -p $LOG_PATH

source pre_construction.sh 2>&1 | tee $LOG_PATH/gcc_arm32le_patch.log

echo "------------------------------------------"
echo "Now building the "gcc_arm32le" toolchain ..."
echo "The entire build process takes about 45 minutes (build time is related to machine preformance), you can view the detailed build log in the ${LOG_PATH} file"
source gcc_update_sourcecode.sh
source gcc_arm_linux_release.sh 2>&1 | tee $LOG_PATH/gcc_arm32le_build.log
echo "Build gcc_arm32le toolchain completed!"
