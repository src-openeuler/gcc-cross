#!/bin/bash

set -e
set -x 

readonly ROOT_TOUCH_SRC="$PWD/../../open_source"

if [[ -z "$COMPILER_INFO" ]];then
	source $PWD/../config.xml
fi

#[task 002/007] touch .info files in source code for binutils
find $ROOT_TOUCH_SRC/$BINUTILS/$BINUTILS_DIR -name "*.info"|xargs -i bash -c "touch {}"

touch $ROOT_TOUCH_SRC/$BINUTILS/$BINUTILS_DIR/binutils/doc/binutils.texi

find $ROOT_TOUCH_SRC/$BINUTILS/$BINUTILS_DIR -name "*.1"|xargs -i bash -c "touch {}"

touch $ROOT_TOUCH_SRC/$GCC/$GCC_DIR/gcc/config/arm/iterators.md

find $ROOT_TOUCH_SRC/$GCC/$GCC_DIR -name "*.1"|xargs -i bash -c "touch {}"

find $ROOT_TOUCH_SRC/$GCC/$GCC_DIR -name "*.info"|xargs -i bash -c "touch {}"

touch $ROOT_TOUCH_SRC/$GLIBC/$GLIBC_DIR/locale/programs/*-kw.h

touch $ROOT_TOUCH_SRC/$MPFR/$MPFR_DIR/*.m4
touch $ROOT_TOUCH_SRC/$MPFR/$MPFR_DIR/configure
touch $ROOT_TOUCH_SRC/$MPFR/$MPFR_DIR/Makefile.*
touch $ROOT_TOUCH_SRC/$MPFR/$MPFR_DIR/doc/*
touch $ROOT_TOUCH_SRC/$GMP/$GMP_DIR/*.m4
touch $ROOT_TOUCH_SRC/$GMP/$GMP_DIR/configure
touch $ROOT_TOUCH_SRC/$GMP/$GMP_DIR/Makefile.*
touch $ROOT_TOUCH_SRC/$GMP/$GMP_DIR/doc/*
touch $ROOT_TOUCH_SRC/$MPC/$MPC_DIR/*.m4
touch $ROOT_TOUCH_SRC/$MPC/$MPC_DIR/configure
touch $ROOT_TOUCH_SRC/$MPC/$MPC_DIR/Makefile.*
touch $ROOT_TOUCH_SRC/$MPC/$MPC_DIR/doc/*
touch $ROOT_TOUCH_SRC/$ISL/$ISL_DIR/*.m4
touch $ROOT_TOUCH_SRC/$ISL/$ISL_DIR/configure
touch $ROOT_TOUCH_SRC/$ISL/$ISL_DIR/Makefile.*
