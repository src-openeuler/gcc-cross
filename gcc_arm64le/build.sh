#!/bin/bash

set -e
set -x

readonly LOG_PATH="$PWD/../logs"

source $PWD/../config.xml

[ -e "$LOG_PATH/gcc_arm64le_patch.log" ] && rm $LOG_PATH/gcc_arm64le_patch.log
[ -e "$LOG_PATH/gcc_arm64le_build.log" ] && rm $LOG_PATH/gcc_arm64le_build.log
mkdir -p $LOG_PATH

source pre_construction.sh 2>&1 | tee $LOG_PATH/gcc_arm64le_patch.log

echo "------------------------------------------"
echo "Now building the "gcc_arm64le" toolchain ..."
echo "The entire build process takes about 45 minutes (build time is related to machine preformance), you can view the detailed build log in the ${LOG_PATH} file"
source gcc_update_sourcecode.sh
source gcc_aarch64_linux_release.sh 2>&1 | tee $LOG_PATH/gcc_arm64le_build.log
echo "Build gcc_arm64le toolchain completed!"
