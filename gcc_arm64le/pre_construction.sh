#!/bin/bash

set -e
set -x

readonly ROOT_BUILD_SRC="$PWD/../../open_source"
readonly OUTPUT_INSTALL="$PWD/../../output/gcc_arm64le"

source $PWD/../config.xml

#clear history build legacy files
clean(){
	[ -n "$OUTPUT_INSTALL" ] && rm -rf $OUTPUT_INSTALL
	mkdir -p $OUTPUT_INSTALL
}

#clean the build direcoty
clean

chmod 777 $ROOT_BUILD_SRC -R

# Extract kernel header files
cd $ROOT_BUILD_SRC/$LINUX_KERNEL_64
make headers_install ARCH=arm64 INSTALL_HDR_PATH=$ROOT_BUILD_SRC/$KERNEL_HEADERS_64
cd -
