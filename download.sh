#!/bin/bash
set -e

readonly LIB_PATH="$PWD/../open_source"
source $PWD/config.xml
build_rpmdir=`rpm --eval "%{_builddir}"`
src_rpmdir=`rpm --eval "%{_sourcedir}"`
SRC_BRANCH="openEuler-25.03"

#judge the open_source

[ ! -d "$LIB_PATH" ] && mkdir $LIB_PATH

function delete_dir() {
	while [ $# !=0 ] ; do
		[ -n "$1" ] && rm -rf $1 ; shift; done
	}

cd $LIB_PATH && delete_dir $GCC $BINUTILS $GMP $MPC $MPFR $ISL

pushd $LIB_PATH
function do_patch() {
	pushd $1
    git lfs install
    git lfs pull
	cp -f * $src_rpmdir
	specfile=`ls *.spec`
	rpmbuild -bp $src_rpmdir/$specfile
	PKG=$(echo *.tar.*)
	pkg_dir=${PKG%%.tar.*}
	if [ $1 = "isl" ];then
		cp -a $build_rpmdir/$1/$pkg_dir ./
	else
		cp -a $build_rpmdir/$pkg_dir ./
	fi
	popd
}
echo "Download $GCC" && git clone -b $SRC_BRANCH https://gitee.com/src-openeuler/gcc.git 
do_patch $GCC

echo "Download $GLIBC" && git clone -b $SRC_BRANCH https://gitee.com/src-openeuler/glibc.git 
do_patch $GLIBC

echo "Download $BINUTILS" && git clone -b $SRC_BRANCH https://gitee.com/src-openeuler/binutils.git 
do_patch $BINUTILS

echo "Download $GMP" && git clone -b $SRC_BRANCH https://gitee.com/src-openeuler/gmp.git 
do_patch $GMP

echo "Download $MPFR" && git clone -b $SRC_BRANCH https://gitee.com/src-openeuler/mpfr.git 
do_patch $MPFR

echo "Download $MPC" && git clone -b $SRC_BRANCH https://gitee.com/src-openeuler/libmpc.git 
do_patch $MPC

echo "Download $ISL" && git clone -b $SRC_BRANCH https://gitee.com/src-openeuler/isl.git 
do_patch $ISL

echo "Download $LINUX_KERNEL" && git clone -b 6.4.0-1.0.1 https://gitee.com/openeuler/kernel.git --depth 1
cp -a $LINUX_KERNEL $LINUX_KERNEL_64
